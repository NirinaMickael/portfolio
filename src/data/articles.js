import React from "react";

function article_1() {
	return {
		date: "Aout 2022 - Now",
		title: "Developper Front-end React",
		link:'https://kingatech.pro/',
		description:
			"Responsible for integration and functionality using React styled component at KINGA (start-up dev)",
		keywords: [
			""
		],
		style: `
				.article-content {
					display: flex;
					flex-direction: column;
					align-items: center;
				}

				.randImage {
					align-self: center;
					outline: 2px solid red;
				}
				`,
		body: (
			<React.Fragment>
				<div className="article-content">
					<div className="paragraph">Content of article 1</div>
					<img
						src="https://picsum.photos/200/300"
						alt="random"
						className="randImage"
					/>
				</div>
			</React.Fragment>
		),
	};
}

function article_2() {
	return {
		date: "September 2022",
		title: "Formateur Dev web",
		link:'http://misa-madagascar.com/',
		description:
			"Founded in 1996 in the Mathematics and Computer Science Department of the Faculty of Science at the University of Antananarivo.Founded with the help of the PRESUP project (Programme de Renforcement de l'Enseignement Supérieur) and the Coopération Française, MISA is both an academic and professional training program, designed to enhance students' skills in mathematics and business administration, making the best possible use of the new technologies arising from advances in computing and the Internet.And every year, MISA organizes computer training courses for beginners.",
		style: ``,
		keywords: [
			""
		],
		body: (
			<React.Fragment>
				<h1>Content of article 2</h1>
			</React.Fragment>
		),
	};
}
function article_3() {
	return {
		date: "January 2023 - March 2024",
		title: "Developper ThreeJs/ MEAN stack",
		link:'https://www.wave.art/',
		description:
			"The Heartishow project includes immersive animation with Three.js, a solid Angular BackOffice with NgRx and Express.ts, Stripe API and Shopify API integration, all wrapped up in a carefully designed user interface with Tailwind.",
		style: ``,
		keywords: [

			""
		],
		body: (
			<React.Fragment>
				<h1>Content of article 3</h1>
			</React.Fragment>
		),
	};
}
const myArticles = [article_1, article_2,article_3];

export default myArticles;
