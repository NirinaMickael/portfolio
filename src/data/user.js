const INFO = {
	main: {
		title: "Reactfolio by Mickael",
		name: "Mickael T.",
		email: "todisoanirinamickael@gmail.com",
		logo: "../logo.png",
	},

	socials: {
		twitter: "https://twitter.com/",
		github: "https://gitlab.com/NirinaMickael",
		linkedin: "https://www.linkedin.com/in/todisoa-nirina-mickael-7345b2227/",
		instagram: "https://instagram.com/",
		stackoverflow: "https://stackoverflow.com/",
		facebook: "https://facebook.com/",
	},

	homepage: {
		title: "Full-stack Javascript and mobile app developer, and  data scientist.",
		description:
			"I am a backend/front-end developer with expertise in Node.js/reactTs/angular. I have experience in building scalable, secure and reliable web applications using various frameworks and technologies. I enjoy solving complex problems and learning new skills. I am passionate about creating high-quality code that follows best practices and industry standards. I am always looking for new challenges and opportunities to grow as a developer.",
	},

	about: {
		title: "I’m Mickael T. I live in Madagascar, where I design the future.",
		description:
			"I've worked on a variety of projects over the years and I'm proud of the progress I've made. Many of these projects are open-source and available for others to explore and contribute to. If you're interested in any of the projects I've worked on, please feel free to check out the code and suggest any improvements or enhancements you might have in mind. Collaborating with others is a great way to learn and grow, and I'm always open to new ideas and feedback.",
	},

	articles: {
		title: "With an unwavering passion for programming and solid experience in web development",
		description:
			"My career path is marked by expertise in product design, an assertive leadership vision and a genuine passion for technological innovation. Explore my portfolio to discover how I've shaped these areas with hard work and creativity.",
	},

	projects: [
		{
			title: "Gagest",
			description:
				"The Stock Management System is a comprehensive solution designed to streamline inventory management processes for businesses of all sizes.",
			logo: [
				{
					id: 0,
					label: "Angular",
					image: "/angular.svg",
				  },
				  {
					id: 1,
					label: "Mongo",
					image: "/mongo.svg",
				  },
				  {
					id: 2,
					label: "Node",
					image: "/node.svg",
				  },
				  {
					id: 3,
					label: "tailwind",
					image: "/tailwind.svg",
				  }
			],
			linkText: "View Project",
			link: "https://gagest.kingatech.pro/",
		},
		{
			title: "E-commerce 3D",
			description:
				"Explore the future of art with our 3D gallery project powered by Three.js. Immerse yourself in an immersive world where you can admire,",
			logo: [
				{
					id: 0,
					label: "Angular",
					image: "/angular.svg",
				  },
				  
				  {
					id: 1,
					label: "Mongo",
					image: "/mongo.svg",
				  },
				  {
					id: 2,
					label: "Node",
					image: "/node.svg",
				  },
				  {
					id: 3,
					label: "tailwind",
					image: "/tailwind.svg",
				  },
				  {
					id:4,
					label:'threeJs',
					image:'https://upload.wikimedia.org/wikipedia/commons/3/3f/Three.js_Icon.svg'
				  }
			],
			linkText: "View Project",
			link: "https://www.heartishow.com/",
		},
		{
			title: "Ng-aya",
			description:
				"Our labeling tool allows you to contribute to the development of sentiment analysis models by providing labeled data.",
			logo: [
				{
					id: 0,
					label: "Angular",
					image: "/angular.svg",
				  },
				  
				  {
					id: 1,
					label: "Postgresql",
					image: "/postgresql.svg",
				  },
				  {
					id: 2,
					label: "NestJs",
					image: "/node.svg",
				  },
				  {
					id: 3,
					label: "tailwind",
					image: "/tailwind.svg",
				  },
			],
			linkText: "View Project",
			link: "https://ng-aya.netlify.app/",
		},
		{
			title: "MISA website",
			description:
				"I used HTML, CSS, JavaScript and php to implement the visual and functional enhancements.",
			logo:[
				{
					id: 0,
					label: "js",
					image: "/js.svg",
				  },
				  {
					id: 1,
					label: "Css",
					image: "/css.svg",
				  },
				  {
					id: 2,
					label: "Pho",
					image: "/php.svg",
				  }
			],
			linkText: "View Project",
			link: "http://misa-madagascar.com/",
		}
	],
};

export default INFO;
