import React, { useRef } from 'react';
import {  useFrame } from '@react-three/fiber';
// import Roboto from '../Roboto.json';
export function TextMesh({position, color}) {
  const ref = useRef()
  useFrame(() => (ref.current.rotation.x = ref.current.rotation.y += 0.01))

  return (
    <mesh position={position} ref={ref}>
      <boxGeometry args={[1, 1, 1]} attach="geometry" />
      <meshPhongMaterial color={color} attach="material" />
    </mesh>
  )
}