import React from "react";
import {useNavigate } from "react-router-dom";

import "./styles/navBar.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
	faBriefcase,
	faHome,
	faMessage,
	faTools,
	faUser,
} from "@fortawesome/free-solid-svg-icons";
import { CircleMenu, CircleMenuItem } from "react-circular-menu";
const menus = [
	{
		link:'',
		icon:faHome
	},
	{
		link:'projects',
		icon:faTools
	},
	{
		link:'about',
		icon:faUser
	},
	{
		link:'experience',
		icon:faBriefcase
	},
	{
		link:'contact',
		icon:faMessage
	}
]
const NavBar = (props) => {
	const { active } = props;
	const navigate = useNavigate()
	return (
		<React.Fragment>
			<div
				style={{
					marginTop: "100px",
					position: "fixed",
					right: "50px",
					zIndex:66666
				}}
			>
				<CircleMenu
					startAngle={90}
					rotationAngle={270}
					itemSize={2}
					radius={4}
					rotationAngleInclusive={false}
				>
					{
						menus.map((e)=>{
							return <CircleMenuItem
								onClick={()=>{navigate( `/${e.link}`  )}}
								tooltip={e.link}
								tooltipPlacement="left"
								menuActive={active===e.link}
							>
								<FontAwesomeIcon icon={e.icon} />
							</CircleMenuItem>
						})
					}
				</CircleMenu>
			</div>
		</React.Fragment>
	);
};

export default NavBar;
