import React from "react";
import { faBriefcase } from "@fortawesome/free-solid-svg-icons";

import Card from "../common/card";

import "./styles/works.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Works = () => {
	return (
		<div className="works">
			<Card
				icon={faBriefcase}
				title="Work"
				body={
					<div className="works-body">
						<div className="work">
						<FontAwesomeIcon icon={faBriefcase} className="work-image"/>
							<div className="work-title">KINGA (start-up dev)</div>
							<div className="work-subtitle">
								Software Engineer (MERN STACK)
							</div>
							<div className="work-duration"> 2021 - Now </div>
						</div>

						<div className="work">
							<FontAwesomeIcon icon={faBriefcase} className="work-image"/>
							<div className="work-title">Société HONOREE Sarl (Wave.art)</div>
							<div className="work-subtitle">
								Software Engineer (MEAN STACK/ThreeJs)
							</div>
							<div className="work-duration">2023 - 2024</div>
						</div>
					</div>
				}
			/>
		</div>
	);
};

export default Works;
